"""
Pretty matplotlib example.
See also: http://www.loria.fr/~rougier/teaching/matplotlib/
"""
from pylab import figure, subplot, np, plot, gca, xlim, xticks, ylim, yticks, legend, show

figure(figsize=(8, 5), dpi=80)
subplot(111)

X = np.linspace(-np.pi, np.pi, 256, endpoint=True)
COS, SIN = np.cos(X), np.sin(X)

plot(X, COS, color="blue", linewidth=2.5, linestyle="-", label="cosine")
plot(X, SIN, color="red", linewidth=2.5, linestyle="-",  label="sine")

ax = gca() # get current axis
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data', 0))

# pylint: disable=E1103
xlim(X.min() * 1.1, X.max() * 1.1)
xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],
       [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$+\pi/2$', r'$+\pi$'])

ylim(COS.min() * 1.1, COS.max()*1.1)
yticks([-1, +1],
       [r'$-1$', r'$+1$'])

legend(loc='upper left')

# savefig("../figures/exercice_8.png",dpi=72)
show()
