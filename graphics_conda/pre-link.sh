#!/bin/bash
PR_EXISTED=true    # true if ProcessData existed in 00PythonSoftware
EXISTED=true       # true if 00PythonSoftware folder existed in $HOME
# if function exits with error, remove created directories
cleanup() {
    if ! $PR_EXISTED; then
        rm -rf $HOME/00PythonSoftware/Graphics
    fi
    if ! $EXISTED; then
        rm -rf $HOME/00PythonSoftware
    fi
}
trap cleanup ERR
# Yes/No prompt for various questions
promptyn() {
    while true; do
        read -p "$1 (y/N) " yn
        case $yn in 
            [Yy]* ) return 0;;
            * ) return 1;;
        esac
    done
}


echo 
# check if git is installed
if ! hash git 2>/dev/null; then
	echo "It seems that git is not installed. Please use your package manager to install it before restarting the installation."
	echo "For Ubuntu run:  sudo apt-get install git"
	exit 1
fi

# check if 00PythonSoftware folder exists in home directory 
if [ ! -d  "$HOME/00PythonSoftware" ]; then 
    mkdir $HOME/00PythonSoftware
    EXISTED=false
fi

# read the username and clone git repository
if [ ! -d "$HOME/00PythonSoftware/Graphics" ]; then
    PR_EXISTED=false
    read -p "Provide your bitbucket username: " username
    while ! git clone https://$username@bitbucket.org/ufechner/Graphics.git; do :; done
    mv Graphics ~/00PythonSoftware
fi
