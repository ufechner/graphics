Introduction
------------

In this folder you can find a collection of examples how to create nice diagrams with Python.


Matplotlib examples
-------------------

The examples 01 to 11 all use the matplotlib library. It is easy to use and provides high quality
2d and simple 3d diagrams.

If you have a problem to execute example 09 try the following on the command line:

cd /usr/local/lib/python2.7/dist-packages/matplotlib/mpl-data/sample_data
sudo gunzip ct.raw.gz

Mayavi demo
-----------

The script Mayavi_demo_01.py shows a simple example of high quality 3D graphics.
Mayavi is currently not used in the kite-power group. It is probably a good choice to
visualize 3D flow-fields (see: http://code.enthought.com/projects/mayavi/) .

Computer graphics kit
---------------------

For reel-time 3D graphics currently the cgkit library is used (see: http://cgkit.sourceforge.net/) .
Examples of this library can be found in the subfolder cgkit of the (Python-) KiteSim project.

PyQtGraph - Scientific Graphics and GUI Library for Python
----------------------------------------------------------

Probably a good choice to display 2D data in reel-time. Can also display 3D diagrams, but only on
the screen and not using the high quality renderers, that can be used by cgkit.
See: http://www.pyqtgraph.org/

Installation:
sudo pip install -U pyqtgraph

Usage:
http://www.pyqtgraph.org/documentation/index.html

One example in this folder.

WebGUI (GUI in the browser)
---------------------------

For teaching purposes it would be very useful to have a good graphical-user interface in the
browser.

One option is to use the ipython notebook (see: http://ipython.org/notebook.html).
Another option is use "Pyjs" or "Pyjs Desktop" (see:http://pyjs.org/About.html).
