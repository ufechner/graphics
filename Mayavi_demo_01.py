""" 
Advanced 3D graphics. 
You need to install mayavi first: 
> sudo apt-get install mayavi2
"""
# Create the data.
from numpy import pi, sin, cos, mgrid
from mayavi import mlab

def createData():
    dphi, dtheta = pi/250.0, pi/250.0
    [phi, theta] = mgrid[0:pi + dphi * 1.5:dphi, 0:2 * pi + dtheta * 1.5:dtheta]
    radius = sin(4 * phi)**3 + cos(2 * phi)**3 + sin(6 * theta)**2 + cos(6 * theta)**4
    pos_x = radius * sin(phi) * cos(theta)
    pos_y = radius * cos(phi)
    pos_z = radius * sin(phi) * sin(theta)
    return pos_x, pos_y, pos_z

def showData(pos_x, pos_y, pos_z):
    mlab.mesh(pos_x, pos_y, pos_z)
    mlab.show()
    
if __name__ == "__main__":
    x1, y1, z1 = createData()
    showData(x1, y1, z1)
