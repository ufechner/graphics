"""
Very pretty matplotlib example.
See also: http://www.loria.fr/~rougier/teaching/matplotlib/
"""
from pylab import np, plot, show, figure, subplot, gca, xlim, xticks, ylim, yticks, legend, scatter, annotate

figure(figsize=(8, 5), dpi=80)
subplot(111)

X = np.linspace(-np.pi, np.pi, 256, endpoint=True)
COS, SIN = np.cos(X), np.sin(X)

plot(X, COS, color="blue", linewidth=2.5, linestyle="-", label="cosine")
plot(X, SIN, color="red", linewidth=2.5, linestyle="-",  label="sine")

ax = gca()
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data', 0))

# Draw a 15 point long arrow that will always be at the top-middle corner of
# the axes.  The key is that we're specifying a location of (0,1) in
# "axes fraction" coordinates.  We then place an empty text string 12 _points_
# above this (the `xytext` parameter controls the amount, `textcoords` controls
# how it's interpreted).  Then, we draw an arrow connecting the top left corner
# of the axes to the (empty and not drawn) text string.
ax.annotate('', xy=(0.498, 1), xycoords='axes fraction', xytext=(0, 12),
            textcoords='offset points', ha='center',
            arrowprops=dict(arrowstyle='<|-', shrinkA=0, shrinkB=0,
                           facecolor='black'))

# pylint: disable=E1103
xlim(X.min() * 1.1, X.max() * 1.1)
xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],
       [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$+\pi/2$', r'$+\pi$'])

ylim(COS.min() * 1.1, COS.max()*1.1)
yticks([-1, +1],
       [r'$-1$', r'$+1$'])


legend(loc='upper left')

t = 2 * np.pi / 3
plot([t, t], [0, np.cos(t)],
     color ='blue',  linewidth=1.5, linestyle="--")
scatter([t, ], [np.cos(t), ], 50, color ='blue')
annotate(r'$sin(\frac{2\pi}{3})=\frac{\sqrt{3}}{2}$', xy=(t, np.sin(t)),  xycoords='data',
         xytext=(+10, +30), textcoords='offset points', fontsize=16,
         arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"))

plot([t, t], [0, np.sin(t)],
          color ='red',  linewidth=1.5, linestyle="--")
scatter([t, ], [np.sin(t), ], 50, color ='red')
annotate(r'$cos(\frac{2\pi}{3})=-\frac{1}{2}$', xy=(t, np.cos(t)),  xycoords='data',
         xytext=(-90, -50), textcoords='offset points', fontsize=16,
         arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"))

for label in ax.get_xticklabels() + ax.get_yticklabels():
    label.set_fontsize(16)
    label.set_bbox(dict(facecolor='white', edgecolor='None', alpha=0.65 ))

# savefig("../figures/exercice_10.png",dpi=72)
show()
