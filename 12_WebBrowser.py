"""
Example for displaying an interactive diagram in a web-browser.
Does work with a Python shell only, not yet with ipython.
"""

import matplotlib
import numpy as np
matplotlib.use('webagg')
import matplotlib.pyplot as plt

X = np.linspace(-np.pi, np.pi, 256, endpoint=True) # create an array from -pi to pi
COS, SIN = np.cos(X), np.sin(X)
plt.plot(X, COS, label='cos')
plt.plot(X, SIN, label='sin')
plt.show()
