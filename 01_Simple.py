"""
Simple matplotlib example.
See also: http://www.loria.fr/~rougier/teaching/matplotlib/
"""
from pylab import np, plot

X = np.linspace(-np.pi, np.pi, 256, endpoint=True) # create an array from -pi to pi
COS, SIN = np.cos(X), np.sin(X)
plot(X, COS, label='cos')
plot(X, SIN, label='sin')

#savefig("../figures/exercice_1.png",dpi=72)
#show()
