# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 02:02:17 2012

@author: ufechner
"""
from math import sin, cos, pi

print "sin"
print sin(pi)

print "cos"
print cos(pi)